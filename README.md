# ATSAMS70 Documentation

`` !warn! violently incomplete ``

Herein lays some example code for the ATSAMS70, which was used in the [tinynets](https://gitlab.cba.mit.edu/jakeread/tinynets/tree/master) project. Perhaps the register structure you're looking for lurks in the source... Making this available just in case. 

Also includes example circuit.

![schematic](circuit/schematic.png)
![board](circuit/board.png)

![fab](circuit/fab-front.png)